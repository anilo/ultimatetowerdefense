﻿// Followed Tutorial
// http://www.thegamecontriver.com/2014/10/create-sliding-pause-menu-unity-46-gui.html#at_pco=smlwn-1.0&at_si=554fd781c0570b10&at_ab=per-13&at_pos=0&at_tot=1
// 
// AND
// http://www.thegamecontriver.com/2014/09/create-level-select-scroll-menu-unity-46.html
// 

using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LevelMenuScript : MonoBehaviour {

	public GameObject levelLabelPrefab;
	public Canvas myCanvas;

	//refrence for the pause menu panel in the hierarchy
	public GameObject levelMenuPanel;
	//animator reference
	private Animator anim;
	//variable for checking if the game is paused 
	private bool isPaused = false;
	// Use this for initialization
	void Start () {
		//unpause the game on start
		Time.timeScale = 1;
		//get the animator component
		anim = levelMenuPanel.GetComponent<Animator>();
		//disable it on start to stop it from playing the default animation
		anim.enabled = false;
	}
	
	// Update is called once per frame
	public void Update () {
		//pause the game on escape key press and when the game is not already paused
		if(Input.GetKeyUp(KeyCode.Escape) && !isPaused){
			ShowGameLevels();
		}
		//unpause the game if its paused and the escape key is pressed
		else if(Input.GetKeyUp(KeyCode.Escape) && isPaused){
			HideGameLevels();
		}
	}
	
	//function to pause the game
	public void ShowGameLevels(){
		//enable the animator component
		anim.enabled = true;
		//play the Slidein animation
		anim.Play("LevelMenuSliderAnim");
		//set the isPaused flag to true to indicate that the game is paused
		isPaused = true;
		//freeze the timescale
		Time.timeScale = 0;
	}


	//function to unpause the game
	public void HideGameLevels(){
		//set the isPaused flag to false to indicate that the game is not paused
		isPaused = false;
		//play the SlideOut animation
		anim.Play("LevelMenuSliderOutAnim");
		//set back the time scale to normal time scale
		Time.timeScale = 1;
	}


	public void SetActiveGameLevel(int n){
		// start level as passed in the string
		PlayerPrefs.SetInt("activeLevel", n);
		PlayerPrefs.Save();

		StartCoroutine("LaunchLevel", n);

	}

	IEnumerator LaunchLevel(int n) {
		GameObject levelLabel = Instantiate (levelLabelPrefab).gameObject;
		Text levelLabelText = levelLabel.GetComponentInChildren<Text>() as Text;
		levelLabelText.text = "Loading Level " + n;

		levelLabel.transform.SetParent (myCanvas.transform, false);

		yield return new WaitForSeconds(2);
		Application.LoadLevelAsync(Application.loadedLevel);

		//myCanvas.gameObject.AddComponent (levelLabel);
	}
}






//string highScoreKey = "HighScore";
//
//void Start(){
//	//Get the highScore from player prefs if it is there, 0 otherwise.
//	highScore = PlayerPrefs.GetInt(highScoreKey,0);    
//}