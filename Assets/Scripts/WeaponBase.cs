﻿using UnityEngine;
using System.Collections;

public abstract class WeaponBase : MonoBehaviour
{
    public AudioClip screemClip;
    protected int health = 10;
	private  AudioSource myAudioSource;

	void Start()
	{
		myAudioSource = GetComponent<AudioSource>();
	}

    public void RemoveHealth(int numberHealth)
    {
        Debug.Log("Weapon is dying");

        health -= numberHealth;
    }

    void Update()
    {
        if (health <= 0)
        {
            myAudioSource.PlayOneShot(screemClip);
            Destroy(gameObject, screemClip.length);
        }

    }
}
