﻿/* 
	InventoryManager script is responsible for spawning inventory items at each level
	It looks at LevelManager to find out how many & which inventory items should be spawned at current level
	This classs also maintains an array of all the inventory items currently in the game
*/

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine.UI;
using UnityEngine.EventSystems;

// MSDN for Dictionary - https://msdn.microsoft.com/en-us/library/xfhwa508%28v=vs.110%29.aspx


class invButton{
	public GameObject 					myButtonObj;
	public InventoryButtonState			myButtonStateScript;  //this is the script that set various states for the button
	public string						myButtonName;
	public Image						myButtonImage;
	public Button						myButtonScript;
	public Text							myButtonText;
}


public class InventoryManager : MonoBehaviour{
    private Dictionary<string, GameObject> 	dicInventoryInstances;
	private List<invButton> 	listInventoryButtonInstances; 	// Create a pool of reusable buttons that be instantiated and then used for putting in left panel

	public List<string> 		inventoryNames;
	public List<GameObject> 	inventory2DSpriteFalling;
	public Sprite[]				inventory2DSprites;
	public string[] 			inventory3DNamesArray;
	public string				inventoryButtonPrefabName;

	private Dictionary<string, GameObject> dicTowerNameAndInventoryInstances;
    

	private Dictionary<string, invButton> 	dicInventoryButtonsInPanel;   
	// this stores all the objects that are available in the 2D Panel

	private invButton[]						invButtonArray;
	private	int 							nextOpenSlotInInventoryGroup 	= 0;
	private int								maxSlotsInInventoryGroup 		= 5;

	private bool							didPickInventory;
	private string							pickedInventoryName;

//	private string				inventoryNamePrefix = "inventory_";

	private Transform 			spawnPointStart, spawnPointEnd;
	//private RectTransform 	inventoryPanelRectTransform;
	private GameObject			inventoryPanel;

	Vector3 inventoryOffScreenPos;
	
	private static int inventorySpawnCtr = 0;
    
   	void Update () {
	}

	void Awake () {
		NotificationCenter.DefaultCenter().AddObserver(this, "StartInventory");
		pickedInventoryName = null;
		Clicker.instance.OnClickFromClicker += OnClickFromClicker;

		spawnPointStart 		= GameObject.FindGameObjectWithTag("InventorySpawnPointStart").transform;
		spawnPointEnd 			= GameObject.FindGameObjectWithTag("InventorySpawnPointEnd").transform;
		inventoryOffScreenPos	= GameObject.FindGameObjectWithTag("InventoryOffScreenPos").transform.position;
		inventoryPanel			= GameObject.FindGameObjectWithTag("InventoryPanel");
	}
	
	void Start(){
        dicInventoryInstances 				= new Dictionary<string,GameObject>();
		listInventoryButtonInstances		= new List<invButton>();

		dicInventoryButtonsInPanel			= new Dictionary<string,invButton>();
		dicTowerNameAndInventoryInstances 	= new Dictionary<string,GameObject>();
		didPickInventory 					= false;

		StartCoroutine("createEmptyInventoryButtons");
		//OnDrawGizmosSelected();
    }


	IEnumerator createEmptyInventoryButtons(){

		invButton invButtonInstance;

		for (int inventoryGroupCount = 0; inventoryGroupCount < maxSlotsInInventoryGroup; inventoryGroupCount++) {
			yield return new WaitForEndOfFrame();
			invButtonInstance 							= new invButton();
			invButtonInstance.myButtonObj 				= ObjectPool.instance.GetPoolObjectWithName(inventoryButtonPrefabName, false);

			if (invButtonInstance.myButtonObj != null)
			{
				invButtonInstance.myButtonObj.SetActive(true);
				invButtonInstance.myButtonObj.transform.SetParent( inventoryPanel.transform, false);
				invButtonInstance.myButtonStateScript 		= invButtonInstance.myButtonObj.GetComponent<InventoryButtonState>();
				invButtonInstance.myButtonImage				= invButtonInstance.myButtonObj.GetComponent<Image>();
				invButtonInstance.myButtonScript			= invButtonInstance.myButtonObj.GetComponent<Button>();
				invButtonInstance.myButtonText				= invButtonInstance.myButtonObj.GetComponentInChildren<Text>();
			
				listInventoryButtonInstances.Add(invButtonInstance);
			}else{

				Debug.Log("** ERROR ** ERROR ** Im unable to create ObjectPool instance of type = "+inventoryButtonPrefabName); 
			}
		}
	}
	

	void StartInventory (Notification notification ) {
		
		//		Debug.Log("Received notification from sender = " + notification.sender);
		//		if (notification.data == null)
		//			Debug.Log("And the data object was null!");
		//		else
		//			Debug.Log("And it included a data object =  " + notification.data);
		
		StartCoroutine("SpawnInventoryFalling", notification.data as List<InventoryInfo>);
	}
	
	
	IEnumerator SpawnInventoryFalling(List<InventoryInfo> allInventoryInfo)
	{       
		InventoryMovement inventoryMovementScript;
		GameObject inventoryObjInstance;
		
		foreach (InventoryInfo inventoryInfoObj in allInventoryInfo) {			
			for (int inventoryCount = 0; inventoryCount < inventoryInfoObj.m_inventoryCount; inventoryCount++) 
			{
				yield return new WaitForSeconds (inventoryInfoObj.m_inventoryWaitBetween);
				//				Debug.Log ("MAKING inventory = " + prefabInstance.name);
				
				inventoryObjInstance = (GameObject)Instantiate (getInventoryPrefabWithName(inventoryInfoObj.m_inventoryName), spawnPointStart.position, spawnPointStart.rotation);
				
				inventoryObjInstance.name = "inventory_" + inventoryInfoObj.m_inventoryName + "_" + inventorySpawnCtr;
				//				Debug.Log ("Made inventory = " + inventoryInfoObj.m_inventoryName);
				
				inventoryMovementScript = inventoryObjInstance.GetComponent<InventoryMovement>();				
				inventoryMovementScript.startPos 	= 	spawnPointStart.position;
				inventoryMovementScript.endPos	 	= 	spawnPointEnd.position;
				
				dicInventoryInstances.Add (inventoryObjInstance.name, inventoryObjInstance);

				inventorySpawnCtr++;
			}
		}
	}
	




	// This event gets called from Clicker.cs when anyone is tapped on the screen, because we registered ourselves
	// as a listener for such events
	// This will inform our class whenever a inventory is tapped.  
	// 
	void OnClickFromClicker(GameObject g, Vector3 pos)
	{
		if (g != null && gameObject != null)
		{
			// This is the case where an inventory parachute was picked 
			// So we just need to save this inventory in our panel and take it off the screen.
			if (g.name.Contains ("inventory")) 
			{				
				//Debug.Log ("Inventory " + g.name + " was tapped");
				StartCoroutine("MoveAndDestroyInventory",g);
				string inventoryNameMatch = g.name.Split ('_')[1];
				StartCoroutine("SaveInventorySprite", inventoryNameMatch);
			}
			else{
				// this is the case where a tower was tapped
				// so we need to simply add the inventory to our tower
				if (g.name.Contains ("Tower") && didPickInventory == true)  
				{				
					Debug.Log ("TOWER --> " + g.name + " was tapped");
					Renderer rend = g.GetComponent<Renderer>();  // TODO: Get rid of this getcomponent calls -> these are super slow.
					Vector3 center = rend.bounds.center;
                    center.y = rend.bounds.max.y;
                    addInventoryToTower(pickedInventoryName, g.name, center, Quaternion.identity);
					didPickInventory = false; ///once inventory has been put in tower, you can simply disable the state

					// then set the inventory status back to recharging
					setStateForInvButton(pickedInventoryName, InventoryButtonState.buttonState.recharging);
                }
			}
		}

	}

	bool setStateForInvButton(string inventoryButtonName, InventoryButtonState.buttonState newState){

		invButton invButtonObj;

		if (dicInventoryButtonsInPanel.TryGetValue (inventoryButtonName, out invButtonObj)) {
			invButtonObj.myButtonStateScript.myButtonState = newState;
			return true;
		}

		return false;
	}
	
	
	IEnumerator MoveAndDestroyInventory(GameObject inventoryObj){
		yield return new WaitForEndOfFrame();
		InventoryMovement inventoryMovementScript = inventoryObj.GetComponent<InventoryMovement>();
		inventoryMovementScript.startPos 	= 	inventoryObj.gameObject.transform.position;
		inventoryMovementScript.endPos	 	= 	inventoryOffScreenPos;    //inventoryPanel.position;
		DestroyInventory(inventoryObj.name, 3);
	}
	

	IEnumerator SaveInventorySprite(string inventoryName){
		// TODO: ENSURE THIS CODE IS SYNCHRONIZED - BECAUSE YOU DONT WANT TO CREATE TWO inventory GROUPS OF THE SAME KIND
		yield return new WaitForEndOfFrame();
		//	Debug.Log ("Inserting inventoryNameMatch = "+ inventoryNameMatch);

		if (dicInventoryButtonsInPanel.ContainsKey(inventoryName)){
			Debug.Log ("This type of inventory already exists");
		}else
		{
			if (nextOpenSlotInInventoryGroup < maxSlotsInInventoryGroup) { 
				// get the next available empty button as per the available slot
				invButton invButtonObj = listInventoryButtonInstances[nextOpenSlotInInventoryGroup];
//				GameObject inventoryObj = invButtonObj.myButtonObj;
				invButtonObj.myButtonName = inventoryName;

				// Update the image for the button
				invButtonObj.myButtonImage.sprite = getInventorySpriteWithName(inventoryName);

				// set the onclick action listener for the button
				invButtonObj.myButtonScript.onClick.AddListener(() => InventoryTappedInPanel(inventoryName));

				// Set the state to recharging
				invButtonObj.myButtonStateScript.myButtonState = InventoryButtonState.buttonState.recharging;

				// save the button in my panel dictionary to manage state later on
				dicInventoryButtonsInPanel.Add(inventoryName,invButtonObj);

				nextOpenSlotInInventoryGroup++; //increment counter to point to next open slot

	//			Set the alpha to be fully visible
	//			Color tempColor = imageObj.color;
	//			tempColor.a = 255f;
	//			imageObj.color = tempColor;
	        }
			else {
				Debug.Log ("Max slots reached");
			}
		}
	}

	
	public void InventoryTappedInPanel(string inventoryName){
		Debug.Log ("Inventory Tapped In Panel " + inventoryName);
		pickedInventoryName = inventoryName;
		didPickInventory = true;
        NotificationCenter.DefaultCenter().PostNotification(this,"PickedInventory",inventoryName);
	}
	

	public void DestroyInventory(string inventoryName, float afterSeconds){

		GameObject inventoryObj;

		if (dicInventoryInstances.TryGetValue (inventoryName, out inventoryObj)) {
			dicInventoryInstances.Remove(inventoryName);
			Destroy (inventoryObj, afterSeconds);
		}
	}


	void OnDestroy(){
		NotificationCenter.DefaultCenter().RemoveObserver(this, "StartInventory");  
		Clicker.instance.OnClickFromClicker -= OnClickFromClicker;
    }

	GameObject getInventoryPrefabWithName(string inventoryName){
		int pos = inventoryNames.IndexOf(inventoryName);
		return inventory2DSpriteFalling[pos];      
    }

	Sprite getInventorySpriteWithName(string inventoryName){
		int pos = inventoryNames.IndexOf(inventoryName);
		return inventory2DSprites[pos];
    }
	


	private string getRandomInventoryName (){
		
		int rand = Random.Range (0, (inventory3DNamesArray.Length - 1));
		return inventory3DNamesArray[rand];
	}

	// Once an inventory is picked, it is tracked by two variables
	// private bool							didPickInventory;
	// private string							pickedInventoryName;
	// This method is called when the player is trying to place an inventory on top of the tower
	public GameObject addInventoryToTower(string inventoryName, string towerName, Vector3 atPosition, Quaternion atRotation)
	{
		// TODO:  check if a weapon already exists on tower
		//		GameObject towerObj = getInventory (towerName);
		//
		//		if (!towerObj) 
		//		{
		string randomInventoryName = getRandomInventoryName();
		GameObject obj = ObjectPool.instance.GetPoolObjectWithName(randomInventoryName,false);
		if (obj != null)
		{
			obj.SetActive (true);
			obj.transform.rotation = atRotation;
			obj.transform.position = atPosition;

			dicTowerNameAndInventoryInstances.Add (towerName, obj);
		}
		else
			Debug.LogError ("***ERROR** Could not create instance of: " + randomInventoryName);


		return obj;
		//		} 
		//		else
		//			return null;
	}
	
	
	private GameObject getInventory (string towerName)
	{
		GameObject weaponObj;
		
		if (dicTowerNameAndInventoryInstances.TryGetValue(towerName, out weaponObj))
		{
			return weaponObj;
        }
        else
        {
            //Debug.Log("No I don't have it");
            return null;
        }
    }
    

	/// <summary>
	/// 
	/// </summary>
	/// <returns><c>true</c>, if weapon from tower was removed, <c>false</c> otherwise.</returns>
	/// <param name="towerName">Tower name.</param>
    
    public bool removeInventoryFromTower(string towerName)
    {
        GameObject weaponObj;
        
        weaponObj = getInventory (towerName);
        
        if (weaponObj) {
            ObjectPool.instance.returnToPoolObject(weaponObj);
            
            return dicTowerNameAndInventoryInstances.Remove (towerName);
        } else {
            Debug.Log (towerName);
            
            Debug.Log ("trying to delete weapon, but there is nothing to delete");
			return false;
		}
	}


}
	/// <summary>
	/// ///////
	/// </summary>
	/// <returns>The d_increment coin counter.</returns>
	/// <param name="inventoryName">Inventory name.</param>


//	IEnumerator NOTUSED_incrementCoinCounter(string inventoryName){
//		
//		yield return new WaitForEndOfFrame();
//		invButton inventoryButtonObj;
//		
//		string inventoryNameMatch = inventoryName.Split ('_')[1];
//		
//		//		Debug.Log("Im looking for inventoryNameMatch = " + inventoryNameMatch + " where inventoryName being split = "+ inventoryName);
//		
//		// if a inventory group exists let's increment the counter
//		if (dicInventoryButtonsInPanel.TryGetValue (inventoryNameMatch, out inventoryButtonObj)) {
//			Text textBox = inventoryButtonObj.myButtonText;
//			int ctr = System.Int32.Parse(textBox.text);  
//			ctr++;
//			textBox.text = ctr.ToString();
//		}
//		else{ // if a box group does not exist for this inventory let's create a new one
//			
//			//StartCoroutine("insertNewInventory", inventoryNameMatch);
//		}
//	}
//
//
//}

///
/////
/// //////
/// METHOD WORKS - NOT USING CURRENTLY 
/// 
//IEnumerator createEmptyInventoryButtons(){
//	//GameObject inventoryGroupInstance;	
//	GameObject buttonObj, overlayImageObj;
//	//		float heightOfInventoryPrefab = inventoryPrefabGroup.GetComponent<RectTransform>().rect.height;
//	
//	for (int inventoryGroupCount = 0; inventoryGroupCount < maxSlotsInInventoryGroup; inventoryGroupCount++) {
//		yield return new WaitForEndOfFrame();
//		//	Debug.Log ("MAKING inventory = " + prefabInstance.name);
//		
//		buttonObj = CreateButton(inventoryPanel.transform, Vector3.zero, Vector2.one, null, true, null);
//		
//		overlayImageObj = CreateImage(buttonObj.transform, Vector3.zero, Vector2.one, null);
//		
//		//buttonObj.name = inventoryNamePrefix + inventoryGroupCount;
//		inventoryGroups[inventoryGroupCount]= buttonObj;
//		//			Debug.Log ("Made inventory = " + inventoryInfoObj.m_inventoryName);
//	}
//}
//

///
/////
/// //////
/// METHOD WORKS - NOT USING CURRENTLY 
/// 
//private GameObject CreateButton(Transform parentTransform ,Vector3 position, Vector2 size, UnityEngine.Events.UnityAction method, bool addImage, Sprite imageSprite)
//{
//	GameObject button = new GameObject();
//	button.transform.SetParent( parentTransform, false);
//	button.AddComponent<RectTransform>();
//	button.AddComponent<Button>();
//	button.transform.position = position;
//	button.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, 1);
//	
//	if (method !=null)
//		button.GetComponent<Button>().onClick.AddListener(method);
//	
//	if (addImage == true){
//		button.AddComponent<Image>();
//		Image imgScript = button.GetComponent<Image>();
//		imgScript.preserveAspect = true;
//	}
//	
//	if (addImage==true & imageSprite != null)
//		button.GetComponent<Image>().sprite = imageSprite;
//	
//	return button;
//}
//

//	private GameObject CreateImage(Transform parentTransform ,Vector3 position, Vector2 size, Sprite imageSprite)
//	{
//		GameObject imageObj = new GameObject();
//		imageObj.transform.parent = parentTransform;
//		imageObj.AddComponent<RectTransform>();
//		imageObj.transform.position = position;
//		imageObj.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, 1);
//		imageObj.AddComponent<Image>();
//		Image imgScript = imageObj.GetComponent<Image>();
//		imgScript.preserveAspect = true;
//		
//		if (imageSprite != null)
//			imgScript.sprite = imageSprite;
//        
//		return imageObj;
//    }
//    

