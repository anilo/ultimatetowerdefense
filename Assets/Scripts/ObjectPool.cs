﻿/// <summary>
/// ObjectPool can pre-create multiple object types and keep them ready for usage later in the game
/// Simply apply the Object Pool to one of your objects that runs forever in your Level/game
/// Then simply call it to get your instance
/// 
/// This code was adapted from these two links: 
/// Primary : http://forum.unity3d.com/threads/simple-reusable-object-pool-help-limit-your-instantiations.76851t
/// For inspiration: http://answers.unity3d.com/questions/765574/can-someone-just-post-a-generic-object-pool-script.html
/// 
/// Notice that the Primary link provides most of the logic, but it keeps destroying these objects when not in use and then recreating them 
/// What we want to do is simply disable the objects, then return it back to pool - as suggested by the Unity Tutorial
/// 
/// The logic below is 
/// 
/// 
// What it does:
//	
// The object pool allow you to buffer or pool objects you plan on reusing many time throughout your game, by allowing you to request a new 
// object from it, and tell it whether or not it must give you an object or only give you an object thats available. 
// It also buffers objects at the start to give you a pool to work with initially.
//		
//		How to use:
//		
//		1. Add the ObjectPool.cs behavior to a GameObject.
//		2. In the object prefabs array set the prefabs you want to be pooled and reused throughout the game.
//		3. In the amount to buffer array, specify how many of each object you want to instantiate on the scene start so you have pooled objects 
//             to start with. If you don't want to specify each, the default buffer amount will just be used.
//      4. In your game call ObjectPool.instance.GetObjectForType(objectType,onlyPooled).
//         For the object type just give the name of the prefab you want to spawn. 
//         Specify true or false for the onlyPooled value, if true it will only return a GameObject if there is already an object pooled, 
//         if false it will instantiate a new object if one is not available. (Set true if you want to limit how many objects are pooled, 
//         very helpful if you want a limited number of effects to ever happen at one time).
//      5. Make sure your Gameobject you get from the pool is disabled once you are done using it - Simply SetActive(false) on the 
//         object and it will be back in the pool for use again
//
//
//     I have also included the Effect.cs and SoundEffect.cs I use in my game to get anyone started right away. 
//     The Effect.cs behavior should be added to a game object, then have any number of ParticleEmitters added to it which will 
//     run a single emit each time StartEffect is called. Then after a set amount of time it will reset the effect and pool itself. 
//     SoundEffect, does a similar thing but with sound effects.
/// 
/// </summary>
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ObjectPool : MonoBehaviour
{
	
	public static ObjectPool instance;
	
	/// <summary>
	/// The object prefabs which the pool can handle.
	/// </summary>
	public GameObject[] objectPrefabs;
	
	/// <summary>
	/// The pooled objects currently available.
	/// </summary>
	public List<GameObject>[] pooledObjects;
	
	/// <summary>
	/// The amount of objects of each type to buffer.
	/// </summary>
	public int[] amountToBuffer;
	
	public int defaultBufferAmount = 3;
	
	/// <summary>
	/// The container object that we will keep unused pooled objects so we dont clog up the editor with objects.
	/// </summary>
	protected GameObject containerObject;
	
	void Awake ()
	{
		instance = this;
	}
	
	// Use this for initialization
	void Start ()
	{
		containerObject = new GameObject("ObjectPool");
		
		//Loop through the object prefabs and make a new list for each one.
		//We do this because the pool can only support prefabs set to it in the editor,
		//so we can assume the lists of pooled objects are in the same order as object prefabs in the array
		pooledObjects = new List<GameObject>[objectPrefabs.Length];
		
		int i = 0;
		foreach ( GameObject objectPrefab in objectPrefabs )
		{
			pooledObjects[i] = new List<GameObject>(); 
			
			int bufferAmount;
			
			if(i < amountToBuffer.Length) bufferAmount = amountToBuffer[i];
			else
				bufferAmount = defaultBufferAmount;
			
			for ( int n=0; n<bufferAmount; n++)
			{
				GameObject newObj = Instantiate(objectPrefab) as GameObject;
				newObj.name = objectPrefab.name;
				PoolObject(newObj);
			}
			
			i++;
		}
	}
	
	/// <summary>
	/// Gets a new object for the name type provided.  If no object type exists or if onlypooled is true and there is no objects of that type in the pool
	/// then null will be returned.
	/// </summary>
	/// <returns>
	/// The object for type.
	/// </returns>
	/// <param name='objectType'>
	/// Object type.
	/// </param>
	/// <param name='onlyPooled'>
	/// If true, it will only return an object if there is one currently pooled.
	/// </param>
	public GameObject GetPoolObjectWithName ( string objectNameStr , bool onlyPooled )
	{
		int i=0;
		 
		foreach ( GameObject prefab in objectPrefabs )
		{
//			GameObject prefab = objectPrefabs[i];
			if(prefab.name == objectNameStr)
			{
				// Lets look for an inactive object 
				foreach (GameObject poolObj in pooledObjects[i])
				{
					if(!poolObj.activeInHierarchy)
				    {
//						poolObj.transform.SetParent(null, false);
						//pooledObject.SetActive(true);
						return poolObj;
					}   
				}

				// code reaches here if prefab name matches but none of the pooled instances are inactive
				// therefore we need to create a new instance to keep the game going
				if(!onlyPooled) {
					GameObject newObj = Instantiate(prefab) as GameObject;
					newObj.name = prefab.name;
                    PoolObject(newObj);
					return newObj;
				}
			}

			i++;
		}
		
		//If we have gotten here either there was no object of the specified type or non were left in the pool with onlyPooled set to true
		return null;
	}
	
	/// <summary>
	/// Pools the object specified.  Will not be pooled if there is no prefab of that type.
	/// </summary>
	/// <param name='obj'>
	/// Object to be pooled
	/// </param>
	private bool PoolObject ( GameObject obj )
	{
		int i=0;
		foreach ( GameObject prefab in objectPrefabs )
		{
			if(prefab.name == obj.name)
			{
				obj.SetActive(false);
				obj.transform.SetParent(containerObject.transform, false); // The containerobject is simply a place where all pooled objects are parked
				pooledObjects[i].Add(obj);
				return true;
			}
			i++;
		}
		return false;
	}


	public bool returnToPoolObject( GameObject obj )
	{
		foreach ( GameObject prefab in objectPrefabs )
		{
			if(prefab.name == obj.name)
			{
				obj.SetActive(false);
				obj.transform.SetParent(containerObject.transform, false); // The containerobject is simply a place where all pooled objects are parked
                return true;
			}
		}
		return false;
	}


}



//using UnityEngine;
//using System.Collections;

//public class Effect : MonoBehaviour
//{
//	/// <summary>
//	/// The array of emitters to fire when the effect starts.
//	/// </summary>
//	public ParticleEmitter[] emitters;
//	
//	/// <summary>
//	/// The length of the effect in seconds.  After which the effect will be reset and pooled if needed.
//	/// </summary>
//	public float effectLength = 1f;
//	
//	
//	/// <summary>
//	/// Should the effect be added to the effects pool after completion.
//	/// </summary>
//	public bool poolAfterComplete = true;
//	
//	
//	
//	/// <summary>
//	/// Resets the effect.
//	/// </summary>
//	public virtual void ResetEffect ()
//	{
//		if(poolAfterComplete)
//		{
//			ObjectPool.instance.PoolObject(gameObject);
//		} else {
//			Destroy(gameObject);
//		}
//	}
//	
//	/// <summary>
//	/// Starts the effect.
//	/// </summary>
//	public virtual void StartEffect ()
//	{
//		foreach ( ParticleEmitter emitter in emitters )
//		{
//			emitter.Emit();
//		}
//		
//		StartCoroutine(WaitForCompletion());
//	}
//	
//	public IEnumerator WaitForCompletion ()
//	{
//		//Wait for the effect to complete itself
//		yield return new WaitForSeconds(effectLength);
//		
//		//Reset the now completed effect
//		ResetEffect();
//		
//	}
//	
//	
//	
//}
//
//
//
////using UnityEngine;
////using System.Collections;
//
//public class SoundEffect : MonoBehaviour
//{
//	
//	/// <summary>
//	/// The sound source that will be played when the effect is started.
//	/// </summary>
//	public AudioSource soundSource;
//	
//	/// <summary>
//	/// The sound clips that will randomly be played if there is more than 1.
//	/// </summary>
//	public AudioClip[] soundClips;
//	
//	/// <summary>
//	/// The length of the effectin seconds.
//	/// </summary>
//	public float effectLength = 1f;
//	
//	/// <summary>
//	/// Should the effect be pooled after its completed.
//	/// </summary>
//	public bool poolAfterComplete = true;
//	
//	
//	
//	/// <summary>
//	/// Resets the effect.
//	/// </summary>
//	public virtual void ResetEffect ()
//	{
//		if(poolAfterComplete)
//		{
//			ObjectPool.instance.PoolObject(gameObject);
//		} else {
//			Destroy(gameObject);
//		}
//	}
//	
//	/// <summary>
//	/// Starts the effect.
//	/// </summary>
//	public virtual void StartEffect ()
//	{
//		soundSource.PlayOneShot(soundClips[Random.Range(0,soundClips.Length)]);
//		
//		StartCoroutine(WaitForCompletion());
//	}
//	
//	public IEnumerator WaitForCompletion ()
//	{
//		//Wait for the effect to complete itself
//		yield return new WaitForSeconds(effectLength);
//		
//		//Reset the now completed effect
//		ResetEffect();
//		
//	}
//	
//	
//}

