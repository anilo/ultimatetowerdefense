using UnityEngine;
using System.Collections;

public class HeroAI : MonoBehaviour {
	public float walkSpeed = 2f;                        // The nav mesh agent's speed when patrolling.
	public float runSpeed  = 5f;                         // The nav mesh agent's speed when chasing.
	public float chaseWaitTime = 5f;                      // The amount of time to wait when the last sighting is reached.
	public float patrolWaitTime = 1f;                     // The amount of time to wait when the patrol way point is reached.
	public float stoppingDistance = 7f;                     // Nav distance


	enum playerState {attacking, hit, idle, running, walking, death};
	private playerState myPlayerState;

	private Transform mazeEndPoint;	
	public GameObject selectionCircleObj;
	private GameObject victim;
	
	//private var enemySight : EnemySight;                        // Reference to the EnemySight script.
	private NavMeshAgent nav;                             // Reference to the nav mesh agent.
	//private var player : Transform;                             // Reference to the player's transform.
	//private var playerHealth : PlayerHealth;                    // Reference to the PlayerHealth script.
	//private var lastPlayerSighting : LastPlayerSighting;        // Reference to the last global sighting of the player.
	//private var chaseTimer : float;                             // A timer for the chaseWaitTime.
	private float patrolTimer;                            // A timer for the patrolWaitTime.
	private int wayPointIndex;                            // A counter for the way point array.
	private bool playerTapped;


	public GameObject sightBounds;						//trigger for sight bounds
	public GameObject attackBounds;						//trigger for attack bounds
	private TriggerParent sightTrigger;
	private TriggerParent attackTrigger;
	public float chaseStopDistance = 0.7f;
	public bool shouldChase = true;
	public int 	attackDmg = 40;
	private bool isAttacking;


	private Health myHealth;
    
    
	//
	// CALLED EVEN IF GAMEOBJECT IS NOT ENABLED -> ONLY CALLED ONCE 
	void Awake ()
	{
	////    // Setting up the references.
	////    enemySight = GetComponent(EnemySight);
	////    player = GameObject.FindGameObjectWithTag(Tags.player).transform;
	////    playerHealth = player.GetComponent(PlayerHealth);
	////    lastPlayerSighting = GameObject.FindGameObjectWithTag(Tags.gameController).GetComponent(LastPlayerSighting);

	}
	
	private HeroManagerScript heroManagerScript;

	private Animation myAnimations;
	private AudioSource myAudioSource;
	
	public AudioClip[] audioClips;
	private bool nearbyEnemyFound;


	// Use this for initialization
	// CALLED ONLY IF SCRIPT IS ENABLED --> ONLY CALLED ONCE EVEN IF ENABLED/DISABLED MULTIPLE TIMES
	void Start () {
		isAttacking			= false;
		myHealth 			= GetComponent<Health>();
		GameObject gameMgr 	= GameObject.FindGameObjectWithTag ("GameManager");
		heroManagerScript 	= gameMgr.GetComponent<HeroManagerScript>();
//		mazeEndPoint 		= enemyManagerScript.mazeEndPoint;
		nav 				= GetComponent<NavMeshAgent>();
		nav.stoppingDistance = stoppingDistance;

		myAnimations 		= GetComponent<Animation>();
//		myAudioSource 		= GetComponent<AudioSource>();
//		nav.speed = patrolSpeed;

		nearbyEnemyFound = false;

		playerTapped = false;
		// Start the event listener
		Clicker.instance.OnClickFromClicker += OnClickFromClicker;
		selectionCircleObj.SetActive(false);

		//Setup the sightBounds for character
		if (sightBounds) 
		{
			sightTrigger = sightBounds.GetComponent<TriggerParent> ();
			if (!sightTrigger)
				Debug.LogError ("'TriggerParent' script needs attaching to enemy 'SightBounds'", sightBounds);
		} else 
		{
			Debug.LogWarning ("Assign a trigger with 'TriggerParent' script attached, to 'SightBounds' or enemy will not be able to see", transform);
		}
		
		// setup the attackbounds for character.
		if(attackBounds)
		{
			attackTrigger = attackBounds.GetComponent<TriggerParent>();
			if(!attackTrigger)
				Debug.LogError("'TriggerParent' script needs attaching to enemy 'attackBounds'", attackBounds);
		}
		else
			Debug.LogWarning("Assign a trigger with 'TriggerParent' script attached, to 'AttackBounds' or enemy will not be able to attack", transform);
	}

	
	// This event gets called from Clicker.cs when anyone is tapped on the screen, because we registered ourselves
	// as a listener for such events
	// This will move the hero to the right position when we tap on the terrain again.
	// We can also tap on the hero again and this will reset the selection of the player. 
	void OnClickFromClicker(GameObject g, Vector3 pos)
	{
		// If g is THIS gameObject
		//if (g == gameObject)
		//{
//		Debug.Log("Hide :" + g.name);
		if (g != null && gameObject != null)
		{
			if (g.name.Contains (gameObject.name)) 
			{				
				if (playerTapped == false) 
				{
					playerTapped = true;
					selectionCircleObj.SetActive(true);
					Debug.Log ("Player " + gameObject.name+ " was tapped");
				} 
				else 
				{
					playerTapped = false;
					selectionCircleObj.SetActive(false);
					Debug.Log ("Player " + gameObject.name+ " was untapped");
				}
			} 
			else 
			{
				if ((playerTapped == true) && (g.name.Contains ("Terrain")))
				{	
					MoveHeroToTappedPosition(pos);
					playerTapped = false;
					selectionCircleObj.SetActive(true);
					//Debug.Log ("Player " + gameObject.name+ " has moved");
				}
			}
		}
	}

	
	// Update is called once per frame
	void Update () {

		if (myPlayerState != playerState.death)
		{
			attackEnemyNearby ();

			playerStateActions ();
		}
	}


	void attackEnemyNearby ()
	{
		if (sightTrigger && sightTrigger.colliding && shouldChase && !attackTrigger.colliding) 
		{
			victim = sightTrigger.hitObject;
			if (victim != null){
				nav.destination = victim.transform.position;
				myPlayerState = playerState.running;
			}

//			if (nav.destination.magnitude > chaseStopDistance)
//				shouldChase = false;
		} else 
		{
			if (attackTrigger && attackTrigger.colliding) 
			{
				myPlayerState = playerState.attacking;

				victim = attackTrigger.hitObject;
				if (victim != null)
				{
					transform.LookAt(victim.transform);
                
                	if (!isAttacking)
						StartCoroutine("dealDamage" );
					
				}
			} 
			else 
			{
				if (nav.remainingDistance >= nav.stoppingDistance) 
				{ // nav.destination == lastPlayerSighting.resetPosition || nav.remainingDistance < nav.stoppingDistance)
					myPlayerState = playerState.walking;
				} else 
				{
					myPlayerState = playerState.idle;
				}
			}
		}
	}

	
	//remove health from object and push it
	IEnumerator dealDamage()
	{
		isAttacking = true;

		Health victimHealth = victim.GetComponent<Health>();

		//deal dmg
		if(victimHealth){
			if (!victimHealth.isDead()){
				//				Debug.Log ("Reducing health for: " + victim.name);
				victimHealth.takeDamage(attackDmg);
			}		
		}

//		while (myAnimations.IsPlaying("attack 1"))
		yield return new WaitForSeconds (1f);
		
		isAttacking = false;

	}
	

	void playerStateActions()
	{
		if (myHealth.getCurHealth() <= 0){
			myPlayerState = playerState.death;
		}

		switch (myPlayerState) 
		{
				case (playerState.idle):
					myAnimations.CrossFade ("idle");
					break;

				case (playerState.walking):
					nav.speed = walkSpeed;
					myAnimations.CrossFade ("walk");
					break;

				case (playerState.hit):
					nav.speed = 0f;
					myAnimations.CrossFade ("hit");
                	break;  
                
            	case (playerState.running):
					nav.speed = runSpeed ;
					myAnimations.CrossFade ("run");
					break;

				case (playerState.attacking):
					//nav.Stop();
					nav.speed = 0f;
					myAnimations.CrossFade ("attack 1");
					//Debug.Log ("Hero in attacking state");
					break;

	
				case (playerState.death):
					nav.Stop();
					myAnimations.CrossFade ("death");
					
					// myAudioSource.PlayOneShot (audioClips [Random.Range (0, (audioClips.Length - 1))]);
					Destroy(gameObject,3);
					break;
		}
	}


	void MoveHeroToTappedPosition (Vector3 tappedPosition) {
			nav.destination = tappedPosition; 		
	}

	void OnDestroy(){
		// Very important to do this so that this dead player does not receive any more events from the Clicker class

		if (Clicker.instance != null)
			Clicker.instance.OnClickFromClicker -= OnClickFromClicker;
		heroManagerScript.DestroyHero(gameObject.name);
	}
	
}