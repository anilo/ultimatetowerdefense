﻿/* 
  This class parses the JSON files
  Then it organizes level data in an internal list:-> m_allLevels

  Then other classes will import the GameManager 

	SCRIPT DOES NOT DESTROY --> IT HAS DONTDESTROYONLOAD ()
*/

using UnityEngine;
using System.Collections;
using System.IO;
using MiniJSON;
using System.Collections.Generic;

// MiniJson using classes with UnityScript ==> https://gist.github.com/Edudjr/cb407c67e76ac36bcfac#file-classminijson-js
// MiniJson parsing using Javascript ==> https://gist.github.com/darktable/3542254
// another nice example of creating wavemanager ==> http://forum.unity3d.com/threads/tower-defense-wave-manager.106167/


public class EnemyInfo
{
	public long m_enemyWaitBefore;
	public long m_enemyWaitBetween;
	public string m_enemyName;   
    public long m_enemyCount; 		
	public long m_enemySpeed;
}

public class HeroInfo
{
	public long m_heroWaitBefore;
	public long m_heroWaitBetween;
    public long m_heroCount; 		
	public long m_heroSpeed;
	public string m_heroName;
}

public class InventoryInfo
{
	public long 		m_inventoryWaitBefore;
    public string 		m_inventoryName; 		
	public long 		m_inventorySpeed;
	public long 		m_inventoryWaitBetween;
	public long 		m_inventoryCount;
}

public class StaticSlotInfo
{
	public long			m_slotNumber;
	public string		m_inventoryInSlot;
}


public class WaveInfo
{
	public List<EnemyInfo> 		m_enemiesList;
	public List<HeroInfo> 		m_heroesList;
	public List<InventoryInfo> 	m_inventoryList;
	public List<StaticSlotInfo> m_staticSlotsList;

	public long m_waveNum;
	public long m_waveDuration;
}


public class LevelInfo
{
	public List<WaveInfo> m_waves;

	public long m_levelNum;
}




public class GameManager : MonoBehaviour {

	private List<LevelInfo> m_allLevels;
	private bool m_parsingCompleted = false;

	private int m_currentLevel;
	private int m_currentWave;

//	private Transform mazeEndPoint;
//	private Transform mazeBeginPoint;
	


	public enum EnemyType
	{
		Enemy01,
		Enemy02,
		Enemy03,
		Enemy04,
	}

//	LevelInfo levelInfoObj;
//	WaveInfo waveInfoObj;
//	EnemyInfo enemyInfoObj;
//	HeroInfo  heroInfoObj;
//	inventoryInfo inventoryInfoObj;


	// Make this game object and all its transform children
	// survive when loading a new scene.
	// 
	// The Awake function is called on all objects in the scene before any object's Start function is called. 
	// This fact is useful in cases where object A's initialisation code needs to rely on object B's already being initialised; 
	// B's initialisation should be done in Awake while A's should be done in Start.
	// 
	// In this case, the parsing of the JSON is done in the Awake method, so that it's the first thing that happens.
	// 
	void Awake () {
		DontDestroyOnLoad (transform.gameObject);
//		Application.LoadLevelAdditive ("PauseMenu");		
		m_parsingCompleted = false;		
		m_allLevels = new List<LevelInfo> ();		
		ParseLevelJSON ();
	}


	// Use this for initialization
	void Start () {
//		mazeBeginPoint 			= GameObject.FindWithTag("mazeBegin").transform;
//		mazeEndPoint 			= GameObject.FindWithTag("mazeEnd").transform;

// Start the event listener
//		Clicker.instance.OnClickFromClicker += OnClickFromClicker;

	

	 	StartCoroutine("StartGame");

		//HeroManagerScript heroMgrScript = GetComponent<HeroManagerScript>();
			
		//printFullJSON ();
	}

	IEnumerator StartGame(){
//		m_currentLevel = GetCurrentLevel (); // PlayerPrefs.GetInt("activeLevel",1);

//		Debug.Log("startiiiiiiiiiiiiiing gameeeeeee");

		yield return new WaitForSeconds (5);
        
//		Debug.Log("LETSSSSS GOOOOOOOO");


        NotificationCenter.DefaultCenter().PostNotification(this, "StartLevel", GetCurrentLevel());  
        
		foreach (WaveInfo waveInfoObj in GetCurrentLevel().m_waves) // for each key/values
		{
//			Debug.Log("IM IN THERE>>>>>");

            NotificationCenter.DefaultCenter().PostNotification(this, "StartWave", waveInfoObj);

			NotificationCenter.DefaultCenter().PostNotification(this, "StartInventory", waveInfoObj.m_inventoryList);
            
			NotificationCenter.DefaultCenter().PostNotification(this, "StartEnemies", waveInfoObj.m_enemiesList);
            
			NotificationCenter.DefaultCenter().PostNotification(this, "StartHeroes", waveInfoObj.m_heroesList);

			NotificationCenter.DefaultCenter().PostNotification(this, "StartStaticSlots", waveInfoObj.m_staticSlotsList);


			yield return new WaitForSeconds (waveInfoObj.m_waveDuration);
        }
    }

	LevelInfo GetLevelInfo(int levelNum)
	{
		if (m_allLevels.Count >= levelNum)
			return m_allLevels[levelNum-1];
		else
			return null;
	}

	public LevelInfo GetCurrentLevel()
	{
		if (isLevelLoadingDone())
		{
			m_currentLevel = PlayerPrefs.GetInt("activeLevel",1);

		//	Debug.Log ("Getting current level = "+ m_currentLevel);
			return GetLevelInfo (m_currentLevel);
		}
		else {
			return null;
		}
	}

//	LevelInfo GetNextLevel()
//	{
//		// Check if next level exists
//		if ((m_currentLevel + 1) <= m_allLevels.Count) {
//						m_currentLevel++;
//						m_currentWave = 1;
//						return m_allLevels [m_currentLevel];
//				} else
//						return null;
//	}

//	public void AddWave(EnemyType enemy, int enemyCount)
//	{
//		WaveInfo info = new WaveInfo() { enemyType = enemy, count = enemyCount };
//		m_waves.Add(info);
//	}
//	
//	public void ResetLevel()
//	{
//		m_currentWaveIndex = 0;
//	}
//	
//	public void NextWave()
//	{
//		m_currentWaveIndex = Mathf.Min(m_currentWaveIndex + 1, m_waves.Count - 1);
//	}
//	
//	public WaveInfo CurrentWave
//	{
//		get
//		{
//			return m_waves[m_currentWaveIndex];
//		}
//	}
//	
//	public int CurrentWaveIndex
//	{
//		get
//		{
//			return m_currentWaveIndex;
//		}
//	}

	private void printFullJSON () {
		foreach (LevelInfo levelInfoObj in m_allLevels) // for each key/values
		{
			Debug.Log("PRINTING FOR LEVEL ==> "+levelInfoObj.m_levelNum);
								
			foreach (WaveInfo waveInfoObj in levelInfoObj.m_waves) // for each key/values
			{
				Debug.Log("PRINTING FOR wave ==> "+waveInfoObj.m_waveNum);
			
			    Debug.Log("PRINTING FOR wave DURATION ==> "+waveInfoObj.m_waveDuration );
					
				foreach (EnemyInfo enemyInfoObj in waveInfoObj.m_enemiesList) // for each key/values
				{
					Debug.Log("PRINTING FOR enemies ==> "+enemyInfoObj.m_enemyName + " : " + enemyInfoObj.m_enemyCount);
				}
				foreach (StaticSlotInfo staticSlotInfoObj in waveInfoObj.m_staticSlotsList) // for each key/values
				{
					Debug.Log("PRINTING FOR static slot info ==> "+staticSlotInfoObj.m_slotNumber + " : " + staticSlotInfoObj.m_inventoryInSlot);
				}
			}
		}
	}





	private void ParseLevelJSON () {
		TextAsset waveInfo = (TextAsset)Resources.Load("waveinfo", typeof(TextAsset));

		StringReader reader = new StringReader(waveInfo.text);
		
		if ( reader == null )
		{
			Debug.Log("WaveInfo.txt not found or not readable");
		}
		else
		{
			Debug.Log("WaveInfo.txt FOUND");
            
            string jsonString;
			
			// Read till end of file
			jsonString = reader.ReadToEnd();
			
			Dictionary<string, object> dict = Json.Deserialize(jsonString) as Dictionary<string, object>;
			
			List<object> allLevelArr = dict["levels"] as List<object>;
			
			
			foreach (Dictionary<string,object> levelInfoDict in allLevelArr) // for each key/values
			{
				LevelInfo levelInfoObj 				= new LevelInfo();
				levelInfoObj.m_waves		= new List<WaveInfo>();


				m_allLevels.Add(levelInfoObj);
				
				levelInfoObj.m_levelNum = (long)levelInfoDict["levelNum"];
				
				List<object> wavesArr 	= levelInfoDict["waves"] as List<object>;

				foreach (Dictionary<string,object> waveInfoDict in wavesArr) // for each key/values
				{
					WaveInfo waveInfoObj 				= new WaveInfo();

					levelInfoObj.m_waves.Add(waveInfoObj);

					
					waveInfoObj.m_waveNum		= (long)waveInfoDict["waveNum"];
					waveInfoObj.m_waveDuration 	= (long) waveInfoDict["waveDuration"];



					/// LOAD ENEMIES ==> 
					/// 
					waveInfoObj.m_enemiesList		= new List<EnemyInfo>();
					List<object> enemiesArr = waveInfoDict["enemies"] as List<object>;
					
					foreach (Dictionary<string,object> enemyInfoDict in enemiesArr) // for each key/values
					{
						EnemyInfo enemyInfoObj	= new EnemyInfo();
						waveInfoObj.m_enemiesList.Add (enemyInfoObj);
						
						enemyInfoObj.m_enemyCount		= (long)enemyInfoDict["count"];
						enemyInfoObj.m_enemySpeed 		= (long)enemyInfoDict["speed"];
						enemyInfoObj.m_enemyName		= (string)enemyInfoDict["enemyName"];
						enemyInfoObj.m_enemyWaitBetween	= (long)enemyInfoDict["waitBetween"];
						enemyInfoObj.m_enemyWaitBefore	= (long)enemyInfoDict["waitBefore"];
                        //Debug.Log("---enemyCount :"+ enemyInfoObj.m_enemyCount);
						//Debug.Log("---enemyName :"+ enemyInfoObj.m_enemyName);
					}

					//LOAD HEROES ==>>
					waveInfoObj.m_heroesList		= new List<HeroInfo>();
					List<object> heroesArr = waveInfoDict["heroes"] as List<object>;
					
					foreach (Dictionary<string,object> heroInfoDict in heroesArr) // for each key/values
					{
						HeroInfo heroInfoObj	= new HeroInfo();
						waveInfoObj.m_heroesList.Add(heroInfoObj);
						
						heroInfoObj.m_heroCount			= (long)heroInfoDict["count"];
						heroInfoObj.m_heroSpeed 		= (long)heroInfoDict["speed"];
						heroInfoObj.m_heroName			= (string)heroInfoDict["heroName"];
                        //Debug.Log("---enemyCount :"+ enemyInfoObj.m_enemyCount);
						//Debug.Log("---enemyName :"+ enemyInfoObj.m_enemyName);
					}


					//LOAD inventoryES ==>>
					List<object> inventoryArr = waveInfoDict["inventory"] as List<object>;

					waveInfoObj.m_inventoryList	= new List<InventoryInfo>();

					foreach (Dictionary<string,object> inventoryInfoDict in inventoryArr) // for each key/values
					{
						InventoryInfo inventoryInfoObj	= new InventoryInfo();
						waveInfoObj.m_inventoryList.Add(inventoryInfoObj);
						
						inventoryInfoObj.m_inventoryCount		= (long)inventoryInfoDict["count"];
						inventoryInfoObj.m_inventorySpeed 		= (long)inventoryInfoDict["speed"];
						inventoryInfoObj.m_inventoryName		= (string)inventoryInfoDict["inventoryName"];
						inventoryInfoObj.m_inventoryWaitBetween	= (long)inventoryInfoDict["waitBetween"];

						//Debug.Log("---enemyCount :"+ enemyInfoObj.m_enemyCount);
						//Debug.Log("---enemyName :"+ enemyInfoObj.m_enemyName);
					}
					//Debug.Log ("For wave : "+waveInfoObj.m_waveNum + ", we have enemies ="+waveInfoObj.m_enemies.Count+ ", we have inventoryes ="+waveInfoObj.m_Allinventory.Count);
				

					//LOAD StaticSlotInfo records ==>>

					if (waveInfoDict.ContainsKey("staticSlots"))
					{
						List<object> staticSlotsArr = waveInfoDict["staticSlots"] as List<object>;
						waveInfoObj.m_staticSlotsList	= new List<StaticSlotInfo>();					
						foreach (Dictionary<string,object> staticSlotsInfoDict in staticSlotsArr) // for each key/values
						{
							StaticSlotInfo staticSlotInfoObj	= new StaticSlotInfo();
							waveInfoObj.m_staticSlotsList.Add(staticSlotInfoObj);						
							staticSlotInfoObj.m_slotNumber		= (long)staticSlotsInfoDict["slotNumber"];
							staticSlotInfoObj.m_inventoryInSlot = (string)staticSlotsInfoDict["inventoryName"];

						}
					}
				
				
				}
			}

			if (m_allLevels.Count >= 1){
				m_parsingCompleted = true;
				Debug.Log ("Parsed the JSON file and got levels = "+m_allLevels.Count); 
			}
			else{
				Debug.Log ("Parsed the JSON file and got ZERO levels");
			}   
        }
	}

	public bool isLevelLoadingDone() {
		return m_parsingCompleted;
	}

}
