﻿using UnityEngine;
using System.Collections;


public class SwipeScript : MonoBehaviour {

	private Transform startMarker;
	public float speed = 0.01f;
	public float swipeDistance = 10;
	public float xMax = 60;
	public float xMin = 0;
	public float yMax = 40;
	public float yMin = -20;
	public float zMax = 60;
	public float zMin = 0;
	
	
	//inside class
	private Vector2 firstPressPos;
	private Vector2 secondPressPos;
	private Vector2 currentSwipe;
	private bool startedLerp;
	
	// final position vector
	private Vector3 endVector;
	
	private float startTime;
	private float journeyLength;


	public void OnGUI() {
		GUI.backgroundColor = Color.black;
		GUI.Button(new Rect(10,10,300,30), "Swipe across screen to see map");
	}


	public void Start()
	{
		startMarker = transform;
		startTime = Time.time;
		endVector = startMarker.position;
		startedLerp = false;
	}
	

	public void startLerping(Vector3 beginMarker, Vector3 finishMarker) 
	{
		journeyLength = Vector3.Distance (beginMarker, finishMarker);
		float distCovered = (Time.time - startTime) * speed;
		float fracJourney = distCovered / journeyLength;
		transform.position = Vector3.Lerp (beginMarker, finishMarker, fracJourney);
		//Debug.Log ("im lerping");
	}

	public void Update()
	{
		if (startedLerp) {
			startLerping (startMarker.position, endVector);
		} 

		if (endVector == startMarker.position)
			startedLerp = false;
	
		if(Input.GetMouseButtonDown(0))
		{
			//save began touch 2d point
			firstPressPos = new Vector2(Input.mousePosition.x,Input.mousePosition.y);
		}

		if(Input.GetMouseButtonUp(0))
		{
			//save ended touch 2d point
			secondPressPos = new Vector2(Input.mousePosition.x,Input.mousePosition.y);
		
			//create vector from the two points
			currentSwipe = new Vector2(secondPressPos.x - firstPressPos.x, secondPressPos.y - firstPressPos.y);
		
			//normalize the 2d vector
			currentSwipe.Normalize();

			//swipe upwards
			if(currentSwipe.y > 0 && currentSwipe.x > -0.5f && currentSwipe.x < 0.5f)
			{		
				//Debug.Log("up swipe");

				if (transform.position.z <= zMax )
				{
					endVector = startMarker.position;
					endVector.z = endVector.z + swipeDistance;
					startedLerp = true;
				}
			}
			//swipe down
			if(currentSwipe.y < 0 && currentSwipe.x > -0.5f && currentSwipe.x < 0.5f)
			{
				//Debug.Log("down swipe");

				if (transform.position.z >= zMin)
				{
					endVector = startMarker.position;
					endVector.z = endVector.z - swipeDistance;
					startedLerp = true;
				}
			}

			//swipe left
			if(currentSwipe.x < 0 && currentSwipe.y > -0.5f && currentSwipe.y < 0.5f)
			{	
				//Debug.Log("left swipe");

				if (transform.position.x >= xMin)
				{
					endVector = startMarker.position;
					endVector.x = endVector.x - swipeDistance;
					startedLerp = true;
				}
			}
			//swipe right
			if(currentSwipe.x > 0 && currentSwipe.y > -0.5f && currentSwipe.y < 0.5f)
			{
				//Debug.Log("right swipe");

				if (transform.position.x <= xMax)
				{
					endVector = startMarker.position;
					endVector.x = endVector.x + swipeDistance;
					startedLerp = true;
				}
			}
		}
	}


}
