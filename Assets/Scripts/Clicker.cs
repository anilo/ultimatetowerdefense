﻿//
// http://unitypatterns.com/singletons/
//

using UnityEngine;
using System.Collections;


public class Clicker : MonoBehaviour 
{
	
	private static Clicker _instance;

	public static Clicker instance
	{
		get
		{
			if(_instance == null)
			{
				_instance = GameObject.FindObjectOfType<Clicker>();

				//Tell unity not to destroy this object when loading a new scene!
				//DontDestroyOnLoad(_instance.gameObject);
			}

			return _instance;
		}
	}

	void Awake() 
	{
		if(_instance == null)
		{
			//If I am the first instance, make me the Singleton
			_instance = this;
			DontDestroyOnLoad(this);
		}
		else
		{
			//If a Singleton already exists and you find
			//another reference in scene, destroy it!
			if(this != _instance)
				Destroy(this.gameObject);
		}
	}

	// Event Handler
	public delegate void OnClickEvent(GameObject g, Vector3 pos);
	public event OnClickEvent OnClickFromClicker;
	
	// Handle our Ray and Hit
	void Update () 
	{
		// Ray
		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

		// Raycast Hit
			RaycastHit hit;
			
		if (Physics.Raycast(ray, out hit, 100))
		{
			// If we click it
			if (Input.GetMouseButtonUp(0))
			{
				// Notify all the listeners about the event!
				// Each listener is then responsible for taking action on it
				OnClickFromClicker(hit.transform.gameObject, hit.point);
				//Debug.Log(hit.transform.gameObject);

			}
		}
	}

}
