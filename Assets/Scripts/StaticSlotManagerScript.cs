﻿/* 
StaticSlotManager script is responsible for filling the empty slots at each level 
It looks at LevelManager to find out how many & which towers should be spawned at current level
*/

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;


// MSDN for Dictionary - https://msdn.microsoft.com/en-us/library/xfhwa508%28v=vs.110%29.aspx

public class StaticSlotManagerScript : MonoBehaviour {
	
	public Dictionary<string, GameObject> dicStaticSlotLocations;

	public List<Transform> 		emptySlots;
	    
	private static int slotSuffix = 0;
	

	// Update is called once per frame
	void Update () {
	}

	void Awake () {
		NotificationCenter.DefaultCenter().AddObserver(this, "StartStaticSlots");
    }
    
    
	void Start()
	{
		dicStaticSlotLocations = new Dictionary<string,GameObject>();
	}

	void StartStaticSlots (Notification notification ) {		
		//		Debug.Log("Received notification from sender = " + notification.sender);
		//		if (notification.data == null)
		//			Debug.Log("And the data object was null!");
		//		else
		//			Debug.Log("And it included a data object =  " + notification.data);

		if (notification.data != null)
			StartCoroutine("SpawnSlots", notification.data as List<StaticSlotInfo>);
	}
    
    
	IEnumerator SpawnSlots(List<StaticSlotInfo> allStaticSlotInfo)
	{
		foreach (StaticSlotInfo slotInfoObj in allStaticSlotInfo) 
		{
				yield return new WaitForSeconds (0);

				GameObject obj = ObjectPool.instance.GetPoolObjectWithName(slotInfoObj.m_inventoryInSlot, false);
				
				if (obj != null)
				{
					obj.SetActive (true);
					obj.transform.rotation = Quaternion.identity;
					obj.transform.position = emptySlots[Mathf.RoundToInt(slotInfoObj.m_slotNumber)].position;
				
					//dicTowerNameAndInventoryInstances.Add (towerName, obj);
            	}
            	else
					Debug.LogError ("***ERROR** Could not create instance of: "+ slotInfoObj.m_inventoryInSlot);
            
//            
//				GameObject gameObj = (GameObject)Instantiate (getTowerPrefabWithName(slotInfoObj.m_inventoryInSlot), 
//			                                  slotInfoObj.m_slotNumber.position, 
//			                                  Quaternion.identity);
//
//				gameObj.name = "mage_" + enemyInfoObj.m_enemyName + "_" + enemiesSuffix;
//				dicEnemyInstances.Add (gameObj.name, gameObj);
//				enemiesSuffix++;
		}
	}



	void OnDestroy(){
		NotificationCenter.DefaultCenter().RemoveObserver(this, "StartStaticSlots");  
	}

}
