﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Health : MonoBehaviour {

	private int myMaxHealth  = 100;
	private int myCurHealth = 100;	
	private Image healthBar;

	
	// Use this for initialization
	void Awake () {
		healthBar = transform.FindChild("HealthBarCanvas").FindChild("HealthBG").FindChild("Health").GetComponent<Image>();	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void takeDamage(int hit){
		if (myCurHealth >0)
		{
			myCurHealth -= hit;
			healthBar.fillAmount = (float)myCurHealth/(float)myMaxHealth;
		}
	}

	public int getCurHealth(){
		return myCurHealth;
	}

	public int getMaxHealth(){
		return myMaxHealth;
	}

	public bool isDead(){
		if (myCurHealth <=0)
			return true;
		else
			return false;
	}

	public void healHealth(int healAmt){

		if (myCurHealth < myMaxHealth)
		{
			myCurHealth += healAmt;
			healthBar.fillAmount = (float)myCurHealth/(float)myMaxHealth;
		}
	}

}
