﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class InventoryButtonState : MonoBehaviour {
	public int 			rechargingSpeed = 10;
	public enum buttonState {selected, available, recharging, disabled};
	public buttonState 		myButtonState;
	private buttonState 	prevButtonState;



	public GameObject 		selectionGameObj;
	public GameObject		rechargingGameObj;

	private Image			rechargingImage;
	private Image			inventoryImage;
	private Button			myButtonScript;
	private bool 			isRecharging;
	private Color			transparentWhiteColor;


	// Use this for initialization
	void Awake () {
		selectionGameObj.SetActive(false);
		isRecharging 								= false;
		myButtonState 								= buttonState.disabled;
		rechargingImage								= rechargingGameObj.GetComponent<Image>();
		inventoryImage								= GetComponent<Image>();
		myButtonScript								= GetComponent<Button>();

		transparentWhiteColor 						= Color.white;
		transparentWhiteColor.a						= 0.5f;

//		NotificationCenter.DefaultCenter().AddObserver(this, "AddedInventoryToTower");

	}


	// In this case when an inventory is added to the tower a notification is sent from InventoryManager.cs to this class.
	// Then in this class, asll we need to do is set the inventory back to reloading state.
	// The variable "notification" is an object of type: String -> and it has the value of the InventoryName
	// that can be used to find the appropriate component in the Inventory Panel and set it back to reload value
	// 
//	void AddedInventoryToTower (Notification notification ) {
//		
//		Debug.Log("Lets reset this inventory -> " + notification.data);
//
//	}



	// Use this for initialization
	void Start () {

	
	}
	
	// Update is called once per frame
	void Update () {
		if (prevButtonState != myButtonState)
			manageButtonStates();
	}

	private void manageButtonStates(){

		if (isRecharging == true)
			return;

		switch (myButtonState) 
		{
		case (buttonState.available):
			selectionGameObj.SetActive(false);
			rechargingGameObj.SetActive(false);
			inventoryImage.color					= Color.white;

			Debug.Log ("Im available");
			break;
			
			
		case (buttonState.recharging):
			selectionGameObj.SetActive(false);
			rechargingGameObj.SetActive(true);
			inventoryImage.color					= transparentWhiteColor;

			if (isRecharging == false){
				isRecharging = true;
				StartCoroutine("StartReloading");
			}

			Debug.Log ("Im recharging");

			break;
			
			
		case (buttonState.disabled):
			selectionGameObj.SetActive(false);
			rechargingGameObj.SetActive(false);
			inventoryImage.color					= transparentWhiteColor;

			Debug.Log ("Im disabled");
			break;

		case (buttonState.selected):
			selectionGameObj.SetActive(true);
			rechargingGameObj.SetActive(false);
			inventoryImage.color					= Color.white;

			Debug.Log ("Im selected");
			break;
		}

		prevButtonState = myButtonState;
	}

	public void setInventoryButtonState(buttonState newButtonState)
	{
		myButtonState = newButtonState;

	}


	IEnumerator StartReloading(){
		float numberOfSteps = 1.0f/rechargingSpeed;
		for (int i=0; i<rechargingSpeed; i++){
			yield return new WaitForSeconds(1f);
			rechargingImage.fillAmount = rechargingImage.fillAmount + numberOfSteps;
		}

//		while( rechargingImage.fillAmount < 1.0f){
//			yield return new WaitForEndOfFrame();
////			yield return new WaitForSeconds(0.1f);
//			rechargingImage.fillAmount = rechargingImage.fillAmount + 0.02f;
//		}
//		
		rechargingImage.fillAmount 	= 0f;
		myButtonState 				= buttonState.available;
		isRecharging 				= false;
	}
}



