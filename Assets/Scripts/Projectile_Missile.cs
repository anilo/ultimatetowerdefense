using UnityEngine;
using System.Collections;


public class Projectile_Missile : MonoBehaviour 
{

	public GameObject myExplosion;
	public Transform myTarget;
	public float myRange = 10;
	public float mySpeed = 10;
	public int myDamage = 2;

	private float myDist;
	private Health enemyHealth;

	
	void Update () 
	{
		transform.Translate(Vector3.forward * Time.deltaTime * mySpeed);
		myDist += Time.deltaTime * mySpeed;
		if(myDist >= myRange)
			Explode();
	
		if(myTarget)
		{
			transform.LookAt(myTarget);
		}
		else
		{
			Explode();
		}
	}

	void OnTriggerEnter(Collider other)
	{
		if(other.gameObject.tag == "Enemy")
		{
			Explode();
		}
	}

	void Explode()
	{
		Instantiate(myExplosion, transform.position, Quaternion.identity);
	
		Vector3 grenadeOrigin = transform.position;
		float radius   = 5.0f;    //provides a radius at which the explosive will effect rigidbodies
//		float power  = 10.0f;    //provides explosive power
//		float explosiveLift = 1.0f; //determines how the explosion reacts. A higher value means rigidbodies will fly upward
//		float explosiveDelay  = 5.0f; //adds a delay in seconds to our explosive object
	
		Collider[] colliders  = Physics.OverlapSphere (grenadeOrigin, radius); //this is saying that if any collider within the radius of our object will feel the explosion
 
		foreach(Collider hit in colliders){  //for loop that says if we hit any colliders, then do the following below
	 		if (hit.gameObject.CompareTag("Enemy")){   			
				enemyHealth = hit.GetComponent<Health>();
				enemyHealth.takeDamage(myDamage);
			}
   		}
     	 
		Destroy(gameObject);
	}
}
