﻿/* 

HeroManager script is responsible for spawning heroes at each level
It looks at LevelManager to find out how many & which heroes should be spawned at current level

This classs also maintains an array of all the heroes currently in the game

*/

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

// MSDN for Dictionary - https://msdn.microsoft.com/en-us/library/xfhwa508%28v=vs.110%29.aspx

public class HeroManagerScript : MonoBehaviour {

	private GameManager myGameManager;

	public Dictionary<string, GameObject> dicHeroInstances;

	public Transform[] heroSpawnPoints;
	
	public List<string> 		heroNames;
	public List<GameObject> 	heroPrefabs;

	private static int heroesSuffix = 0;
	private static int nextHeroPos = 0;


	void Awake () {
		NotificationCenter.DefaultCenter().AddObserver(this, "StartHeroes");
	}

	void Start()
	{
		dicHeroInstances = new Dictionary<string,GameObject>();
	}

	GameObject getHeroPrefabWithName(string heroName){
		
		int pos = heroNames.IndexOf(heroName);
		
		return heroPrefabs[pos];
	}

	void StartHeroes (Notification notification ) {
		
		//		Debug.Log("Received notification from sender = " + notification.sender);
		//		if (notification.data == null)
		//			Debug.Log("And the data object was null!");
		//		else
		//			Debug.Log("And it included a data object =  " + notification.data);
		
		StartCoroutine("SpawnHeroes", notification.data as List<HeroInfo>);
	}
	
	
	IEnumerator SpawnHeroes(List<HeroInfo> allHeroInfo)
	{
		foreach (HeroInfo heroInfoObj in allHeroInfo) 
		{
			//Debug.Log ("Making heroesCount = " + heroInfoObj.m_heroCount);
			for (int heroCount =0; heroCount < heroInfoObj.m_heroCount; heroCount++) 
			{			
				yield return new WaitForEndOfFrame();
				GameObject gameObj = (GameObject)Instantiate (getHeroPrefabWithName(heroInfoObj.m_heroName), heroSpawnPoints[nextHeroPos++].position, Quaternion.identity);
				gameObj.name = "hero_" + heroInfoObj.m_heroName + "_" + heroesSuffix;
				dicHeroInstances.Add (gameObj.name, gameObj);
				heroesSuffix++;
			}
		}
	}

//	public Transform FindRandomEnemy () 
//	{
//		GameObject enemyObject;
//
//		int randomer = Random.Range (0, (dicHeroInstances.Count ));
//
//		string objToFind = "hero" + (randomer-1);
//
//		if (dicEnemyInstances.TryGetValue(objToFind, out enemyObject))
//		{
//			return enemyObject.transform;
//		}
//		else
//		{
//			return null;
//		}
//	}

//	public GameObject FindClosestHero (Vector3 fromPosition)
//	{
//		GameObject closest = null; 
//		float distance = Mathf.Infinity; 
//
//		// return null if no enemies created so far.
//		if (dicEnemyInstances.Count == 0)
//			return null;
//
//		// Iterate through them and find the closest one
//		foreach (GameObject go in dicEnemyInstances.Values)  
//		{ 
//			var diff = (go.transform.position - fromPosition);
//			var curDistance = diff.sqrMagnitude; 
//			if (curDistance < distance) 
//			{ 
//				closest = go; 
//				distance = curDistance; 
//			} 
//		} 
//		//return closest;
//		if (closest)
//			return closest;	
//		else	
//			return null;
//	}


	public void DestroyHero(string enemyName){

		GameObject enemyObj;

		if (dicHeroInstances.TryGetValue (enemyName, out enemyObj)) {

			dicHeroInstances.Remove(enemyName);
			//Destroy (enemyObj);
		}
	}
	

	void OnDestroy(){
		NotificationCenter.DefaultCenter().RemoveObserver(this, "StartHeroes");  
	}
}
