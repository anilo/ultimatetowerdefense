﻿using UnityEngine;
using System.Collections;


public class EnemyAI : MonoBehaviour 
{
	public float walkSpeed = 2f;                        // The nav mesh agent's speed when patrolling.
	public float runSpeed = 6f;                         // The nav mesh agent's speed when chasing.
	public float chaseWaitTime = 5f;                      // The amount of time to wait when the last sighting is reached.
	public float patrolWaitTime = 1f;                     // The amount of time to wait when the patrol way point is reached.
	public float stoppingDistance = 7f;                     // Nav distance
	public float distanceFromMazeEndPoint = 1f;

//	public var patrolWayPoints : Transform[];                   // An array of transforms for the patrol route.

	private Transform mazeBeginPoint;
	private Transform mazeEndPoint ;

	private GameObject victim;
	private Animator myAnimator;

	enum enemyState {attacking, hit, idle, running, walking, death, hitBack, hitFront, runFast, reachedEnd};
	private enemyState myEnemyState;

	private Health myHealth;

	public GameObject sightBounds;						//trigger for sight bounds
	public GameObject attackBounds;						//trigger for attack bounds
	private TriggerParent sightTrigger;
	private TriggerParent attackTrigger;
	public float chaseStopDistance = 0.7f;
	public bool shouldChase = true;
	public int 	attackDmg = 10;
	private bool isAttacking;


//private var enemySight : EnemySight;                        // Reference to the EnemySight script.
	private NavMeshAgent nav ;                             // Reference to the nav mesh agent.
//private var player : Transform;                             // Reference to the player's transform.
//private var playerHealth : PlayerHealth;                    // Reference to the PlayerHealth script.
//private var lastPlayerSighting : LastPlayerSighting;        // Reference to the last global sighting of the player.
//private var chaseTimer : float;                             // A timer for the chaseWaitTime.
	private float patrolTimer ;                            // A timer for the patrolWaitTime.
	private int wayPointIndex ;                            // A counter for the way point array.
//
//
//function Awake ()
////{
////    // Setting up the references.
////    enemySight = GetComponent(EnemySight);
////    player = GameObject.FindGameObjectWithTag(Tags.player).transform;
////    playerHealth = player.GetComponent(PlayerHealth);
////    lastPlayerSighting = GameObject.FindGameObjectWithTag(Tags.gameController).GetComponent(LastPlayerSighting);
//}


    private EnemyManagerScript myEnemyManagerScript;
	private Animation myAnimations;
	private AudioSource myAudioSource;

	public AudioClip[] audioClips;
 

	void Start () 
	{
		// By tag
		myEnemyManagerScript 	= GameObject.FindWithTag("GameManager").GetComponent<EnemyManagerScript>();
	    mazeEndPoint 			= GameObject.FindWithTag("mazeEnd").transform;
	    nav 					= GetComponent<NavMeshAgent>();
		//nav.stoppingDistance = stoppingDistance;

	    myAnimations 			= GetComponent<Animation>();
	    myAudioSource 			= GetComponent<AudioSource>();
		myAnimator				= GetComponent<Animator>();

		myHealth				= GetComponent<Health>();
		isAttacking = false;
        
		nav.destination = mazeEndPoint.position;

		setupSightAndAttackTriggers();
	}


	void setupSightAndAttackTriggers()
	{

		//Setup the sightBounds for character
		if (sightBounds) 
		{
			sightTrigger = sightBounds.GetComponent<TriggerParent> ();
			if (!sightTrigger)
				Debug.LogError ("'TriggerParent' script needs attaching to enemy 'SightBounds'", sightBounds);
		} else 
		{
			Debug.LogWarning ("Assign a trigger with 'TriggerParent' script attached, to 'SightBounds' or enemy will not be able to see", transform);
		}
		
		// setup the attackbounds for character.
		if(attackBounds)
		{
			attackTrigger = attackBounds.GetComponent<TriggerParent>();
			if(!attackTrigger)
				Debug.LogError("'TriggerParent' script needs attaching to enemy 'attackBounds'", attackBounds);
		}
		else
			Debug.LogWarning("Assign a trigger with 'TriggerParent' script attached, to 'AttackBounds' or enemy will not be able to attack", transform);

	}
//
//
	void Update ()
	{
		attackHeroNearby();

		enemyStateActions();


	}
	

	void attackHeroNearby ()
	{
		if (sightTrigger && sightTrigger.colliding && shouldChase && !attackTrigger.colliding) 
		{
			victim = sightTrigger.hitObject;

			if (victim != null)
			{
				nav.destination = victim.transform.position;
				myEnemyState = enemyState.running;
			}
			
			//			if (nav.destination.magnitude > chaseStopDistance)
			//				shouldChase = false;
		} else 
		{
			if (attackTrigger && attackTrigger.colliding ) 
			{
				victim = attackTrigger.hitObject;

				if (victim != null)
				{
					//nav.destination = victim.transform.position;
					nav.Stop(); 

					transform.LookAt(victim.transform);
					myEnemyState = enemyState.attacking;
					//nav.stoppingDistance = stoppingDistance;

					if (!isAttacking){
						StartCoroutine("dealDamage" );
					}
				}
			} 
			else 
			{
				nav.destination = mazeEndPoint.position;
				nav.Resume();

				if (nav.remainingDistance >= nav.stoppingDistance) 
				{ // nav.destination == lastPlayerSighting.resetPosition || nav.remainingDistance < nav.stoppingDistance)				
					myEnemyState = enemyState.walking;

					float distanceToEnd = Vector3.Distance(transform.position, mazeEndPoint.position);

					if (distanceToEnd <= distanceFromMazeEndPoint){
						myEnemyState = enemyState.reachedEnd;
						//Debug.Log ("Enemy reached the end Yo!!!");
					}

				} else 
				{
					// This is the case where the enemy reaches the end -- that's the only time the enemy will be idle. 
					myEnemyState = enemyState.idle;
					Debug.Log ("OH NO - IDLE & Enemy reached the end Yo!!!");

				}
			}
		}
	}

	//remove health from object and push it
	IEnumerator dealDamage()
	{
		isAttacking = true;

		Health victimHealth = victim.GetComponent<Health>();
		
		//deal dmg
		if(victimHealth){
			if (!victimHealth.isDead()){
//				Debug.Log ("Reducing health for: " + victim.name);
				victimHealth.takeDamage(attackDmg);
			}
		}
		
		yield return new WaitForSeconds (2f);
		isAttacking = false;		
	}




	void enemyStateActions()
	{

		if (myHealth.getCurHealth() <= 0){
			myEnemyState = enemyState.death;
		}

		switch (myEnemyState) 
		{
	        case (enemyState.idle):
				myAnimator.SetFloat("speed",0.0f);
				break;

				
			case (enemyState.walking):
				nav.speed = walkSpeed;
				myAnimator.SetFloat("speed",0.3f);
				break;


			case (enemyState.hitFront):
				nav.speed = 0f;
				myAnimator.SetTrigger("hitFront");
				break;


			case (enemyState.hitBack):
				nav.speed = 0f;
				myAnimator.SetTrigger("hitBack");
				break;


			case (enemyState.runFast):
				nav.speed = runSpeed;
				myAnimator.SetFloat("speed",0.9f);
				break;

				
			case (enemyState.running):
				nav.speed = runSpeed ;
				myAnimator.SetFloat("speed",0.6f);
				break;
				

			case (enemyState.attacking):
				//nav.Stop();
				myAnimator.SetFloat("speed",0.0f);
				myAnimator.SetInteger("attackVariation",Random.Range((int)1, (int)4));
                myAnimator.SetTrigger("attack");
				//Debug.Log ("Enemy ATTACK TRIGGER ==> ");
				break;

			case (enemyState.death):
				nav.Stop();  
				myAnimator.SetFloat("speed",0.0f);
				myAnimator.SetTrigger("die");                 
				myEnemyManagerScript.DestroyEnemy(gameObject.name);
				// myAudioSource.PlayOneShot (audioClips [Random.Range (0, (audioClips.Length - 1))]);
				Destroy(gameObject,5);
				break;


			case (enemyState.reachedEnd):
				nav.Stop();  
				myAnimator.SetFloat("speed",0.0f);
//				myAnimator.SetTrigger("die");                 
				myEnemyManagerScript.DestroyEnemy(gameObject.name);
				// myAudioSource.PlayOneShot (audioClips [Random.Range (0, (audioClips.Length - 1))]);
				Destroy(gameObject,2);
				break;




		}
	}

}