﻿using UnityEngine;
using System.Collections;


public class TurretScript : MonoBehaviour {
	public GameObject myProjectile;
	public float reloadTime = 1f;
	public float turnSpeed = 5f;
	public float firePauseTime = .25f;
	public float errorAmount = 0.001f;
	private Transform myTarget ;
	public Transform[] muzzlePositions;
	public Transform pivot_Tilt ;
	public Transform pivot_Pan ;
	public Transform aim_Pan ;
	public Transform aim_Tilt ;

	private Transform shootTarget ;

	private RaycastHit hit;
	private float nextFireTime;
	private EnemyManagerScript enemyManagerScript;
	private AudioSource myAudioSource;

void Start () 
{
	nextFireTime = Time.time;

	// By tag
 	enemyManagerScript = GameObject.FindWithTag("GameManager").GetComponent<EnemyManagerScript>();
    myAudioSource = GetComponent<AudioSource>();
}

void Update () 
{
	GameObject temp = enemyManagerScript.FindClosestEnemy(transform.position);
	
	if (temp.tag == "EnemyNotFound")
	{
			Destroy(temp);
			return;
	}
	else
	{ // if target exists - this stops tracking if target is killed	
		myTarget = temp.transform;

		aim_Pan.LookAt(myTarget);
		aim_Pan.eulerAngles = new Vector3(0, aim_Pan.eulerAngles.y, 0);
		aim_Tilt.LookAt(myTarget);
		
		pivot_Pan.rotation = Quaternion.Lerp(pivot_Pan.rotation, aim_Pan.rotation, Time.deltaTime*turnSpeed);
		pivot_Tilt.rotation = Quaternion.Lerp(pivot_Tilt.rotation, aim_Tilt.rotation, Time.deltaTime*turnSpeed);

		FireProjectile();
	}
}

void OnTriggerEnter(Collider other)
{	
	if(other.gameObject.tag == "Enemy")
	{
		Debug.Log("Detected Enemy");

		//nextFireTime = Time.time+(reloadTime*.5);
		myTarget = other.gameObject.transform;
		shootTarget = other.gameObject.transform;
	}
}

void OnTriggerStay (Collider other) {
	
	if(other.gameObject.tag == "Enemy")
	{
		Debug.Log("Staying in Trigger with Enemy");
//		FireProjectile();
////
////		nextFireTime = Time.time+(reloadTime*.5);
//		myTarget = other.gameObject.transform;
	}
//	
}


void OnTriggerExit(Collider other)
{
	if(other.gameObject.transform == myTarget)
	{
		Debug.Log("exiting trigger");	

		myTarget = null;
		shootTarget = null;
	}
}

void FireProjectile()
{	
	if(Time.time >= nextFireTime)
	{
		int m = Random.Range(0,2);
		GameObject newMissile = Instantiate(myProjectile, muzzlePositions[m].position, muzzlePositions[m].rotation) as GameObject;
		newMissile.GetComponent<Projectile_Missile>().myTarget = myTarget;
		nextFireTime = Time.time+reloadTime; //set a time for reload of the missile
		myAudioSource.Play();
	}
}

}