﻿/* 

EnemyManager script is responsible for spawning enemies at each level
It looks at LevelManager to find out how many & which enemies should be spawned at current level

This classs also maintains an array of all the enemies currently in the game

This class is also referenced by other classes to find closest enemy & destroy enemy

*/

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;


// MSDN for Dictionary - https://msdn.microsoft.com/en-us/library/xfhwa508%28v=vs.110%29.aspx

public class EnemyManagerScript : MonoBehaviour {

//	private GameManager myGameManager;

	public Dictionary<string, GameObject> dicEnemyInstances;

	public List<string> 		enemyNames;
	public List<GameObject> 	enemyPrefabs;
    
	private Transform mazeEndPoint;
	private Transform mazeBeginPoint;
//	public GameObject[] allPrefabEnemies;
	private int rand;
	private static int enemiesSuffix = 0;
	
//	private LevelInfo m_currentLevelInfo;
	private int m_currentWaveNum;
	
	// Update is called once per frame
	void Update () {
	}

	void Awake () {
		NotificationCenter.DefaultCenter().AddObserver(this, "StartEnemies");
		mazeBeginPoint 			= GameObject.FindWithTag("mazeBegin").transform;
		mazeEndPoint 			= GameObject.FindWithTag("mazeEnd").transform;
    }
    
    
	void Start()
	{
		dicEnemyInstances = new Dictionary<string,GameObject>();
	}

	void StartEnemies (Notification notification ) {		
		//		Debug.Log("Received notification from sender = " + notification.sender);
		//		if (notification.data == null)
		//			Debug.Log("And the data object was null!");
		//		else
		//			Debug.Log("And it included a data object =  " + notification.data);
		
		StartCoroutine("SpawnEnemies", notification.data as List<EnemyInfo>);
	}
    
    
	IEnumerator SpawnEnemies(List<EnemyInfo> allEnemyInfo)
	{
		foreach (EnemyInfo enemyInfoObj in allEnemyInfo) 
		{
//			Debug.Log ("Making enemiesCount = " + enemyInfoObj.m_enemyCount);
			for (int enemyCount =0; enemyCount < enemyInfoObj.m_enemyCount; enemyCount++) 
			{
//				randomer = Random.Range (0, 12);
//			
//				yield return new WaitForSeconds (randomer);
				yield return new WaitForSeconds (enemyInfoObj.m_enemyWaitBetween);

				GameObject gameObj = (GameObject)Instantiate (getEnemyPrefabWithName(enemyInfoObj.m_enemyName), mazeBeginPoint.position, Quaternion.identity);

				gameObj.name = "mage_" + enemyInfoObj.m_enemyName + "_" + enemiesSuffix;
				dicEnemyInstances.Add (gameObj.name, gameObj);
				enemiesSuffix++;
			}
		}
	}


	GameObject getEnemyPrefabWithName(string enemyName){		
		int pos = enemyNames.IndexOf(enemyName);
		
		return enemyPrefabs[pos];
	}	


	public Transform FindRandomEnemy () 
	{
		GameObject enemyObject;

		int randomer = Random.Range (0, (dicEnemyInstances.Count ));

		string objToFind = "mage" + (randomer-1);

		if (dicEnemyInstances.TryGetValue(objToFind, out enemyObject))
		{
			return enemyObject.transform;
		}
		else
		{
			return null;
		}
	}

	public GameObject FindClosestEnemy (Vector3 fromPosition)
	{
		GameObject null_closest;

		GameObject closest = null;

		// return null if no enemies created so far.
		if (dicEnemyInstances.Count == 0) 
		{
			null_closest = new GameObject ();
			null_closest.tag = "EnemyNotFound";
			return null_closest;
		}

		float distance = Mathf.Infinity; 

		// Iterate through them and find the closest one
		foreach (GameObject go in dicEnemyInstances.Values)  
		{ 
			var diff = (go.transform.position - fromPosition);
			var curDistance = diff.sqrMagnitude; 
			if (curDistance < distance)
			{ 
				closest = go;
				distance = curDistance; 
			} 
		} 



//		//return closest;
		if (closest)
			return closest;
		else {
			null_closest = new GameObject ();
			null_closest.tag = "EnemyNotFound";
			return null_closest;
		}
	}


	public void DestroyEnemy(string enemyName){

		GameObject enemyObj;

		if (dicEnemyInstances.TryGetValue (enemyName, out enemyObj)) {

			dicEnemyInstances.Remove(enemyName);
			Destroy (enemyObj);

		}

	}

	void OnDestroy(){
		NotificationCenter.DefaultCenter().RemoveObserver(this, "StartEnemies");  
	}

	
//	public void addText()
//	{
//			Text str;
//			str = gameObject.AddComponent<Text>();
//			str.text = "Loading Level " + m_currentLevelInfo.m_levelNum;
////			str.font = myFont;
//	}
}
