﻿using UnityEngine;
using System.Collections;

public class InventoryMovement : MonoBehaviour {
	
	public Vector3 startPos;
	public Vector3 endPos;
	public float speed = 0.03f;
	private float timer = 0.0f;
	
	private float translationTime = 3.0f;
	
	
	
	void Start () {

	}
	
	
	void Update() {
		if (timer < translationTime) {
			transform.position = Vector3.Slerp(startPos, endPos, timer/translationTime);
			timer += Time.deltaTime;
		}
        else if (timer >= translationTime) {
            transform.position = endPos;
        }
    }
}
